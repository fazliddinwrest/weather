import { Component, OnInit } from '@angular/core';
import { SundryService } from 'src/app/shared/services/sundry.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {

  searchInput: string;

  constructor(
    private sundryService: SundryService
  ) { }

  res: any = {}

  ngOnInit(): void {
    this.initWeatherData();
  }

  cities = [
    { name: 'Tashkent' },
    { name: 'Andijan' },
    { name: 'Bukhara' },
    { name: 'Fergana' },
    { name: 'Jizzax' },
    { name: 'Namangan' },
    { name: 'Navoiy' },
    { name: 'Qashqadaryo' },
    { name: 'Karakalpakstan' },
    { name: 'Khorezm' },
    { name: 'Samarqand' },
    { name: 'Sirdaryo' },
    { name: 'Surxondaryo' }
  ]

  onCityClick(name: string): void {
    this.initWeatherData(name);
  }

  onSearchClick(): void {
    if (this.searchInput) {
      this.initWeatherData(this.searchInput);
    }
  }

  initWeatherData(name: string = 'Tashkent'): void {
    this.sundryService.getCoordinates(name).subscribe((coRes: any) => {
      const coOrdinates = coRes[0];
      this.sundryService.getWeatherInfo(coOrdinates.lat, coOrdinates.lon).subscribe((res: any) => {

        const details = {
          cloudy: res.clouds?.all,
          humidity: res.main?.humidity,
          wind: res.wind?.speed,
          pressure: res.main?.pressure
        }
        const info = {
          date: this.getDate(res.dt, res.timezone),
          temp: res.main?.temp,
          name: res.name,
          weatherState: res.weather[0]?.main
        }
        this.sundryService.weatherInfo$.next(info);
        this.sundryService.weatherInfoDetails$.next(details);
      })
    })

  }

  getDate(dt: any, timezone: any) {
    const utc_seconds = parseInt(dt, 10) + parseInt(timezone, 10);
    const utc_milliseconds = utc_seconds * 1000;
    const local_date = new Date(utc_milliseconds).toUTCString();
    return local_date;
  }
}

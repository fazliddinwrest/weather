import { Component, OnInit } from '@angular/core';
import { SundryService } from 'src/app/shared/services/sundry.service';

@Component({
  selector: 'app-weather-info-detail',
  templateUrl: './weather-info-detail.component.html',
  styleUrls: ['./weather-info-detail.component.scss']
})
export class WeatherInfoDetailComponent implements OnInit {

  constructor(
    private sundryService: SundryService
  ) { }

  data: any = {};

  ngOnInit(): void {
    this.initData();  
  }

  initData(): void {
    this.sundryService.weatherInfoDetails$.subscribe((res: any) => {
      if(res) {
        this.data = res;
      }
    });
  }

}

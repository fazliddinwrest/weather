import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherInfoDetailComponent } from './weather-info-detail.component';

describe('WeatherInfoDetailComponent', () => {
  let component: WeatherInfoDetailComponent;
  let fixture: ComponentFixture<WeatherInfoDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherInfoDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherInfoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

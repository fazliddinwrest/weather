import { Component, OnInit } from '@angular/core';
import { SundryService } from 'src/app/shared/services/sundry.service';

@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.scss']
})
export class WeatherInfoComponent implements OnInit {

  
  constructor(
    private sundryService: SundryService
    ) { }
    
    data: any= {};
  ngOnInit(): void {
    this.initData()
  }

  initData(): void {
    this.sundryService.weatherInfo$.subscribe((res: any) => {
      if(res) {
        this.data = res;
      }
    })
  }

}

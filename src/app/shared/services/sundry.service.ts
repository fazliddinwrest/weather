import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { API_KEY } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class SundryService {

  public weatherInfoDetails$ = new BehaviorSubject<any>(null);
  public weatherInfo$ = new BehaviorSubject<any>(null);

  constructor(
    private http: HttpClient
  ) { }

  getCoordinates(name: string): Observable<any> {
    return this.http.get(`https://api.openweathermap.org/geo/1.0/direct?q=${name}&limit=10&appid=${API_KEY}`)
  }

  getWeatherInfo(lat: number, lon: number): Observable<any> {
    return this.http.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${API_KEY}&units=metric`)
  }

}
